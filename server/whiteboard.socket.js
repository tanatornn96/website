/**
 * Created by Tanatorn on 15-08-01.
 */
module.exports = function (io) {

    var whiteboard = io.of('/whiteboard');
    whiteboard.on('connection', function (socket) {
        console.log('whiteboard connected');

        socket.on('pencilDown', function (data) {
            socket.broadcast.emit('pencilDown', data);
        });

        socket.on('pencilUp', function (data) {
            socket.broadcast.emit('pencilUp', data);
        });

        socket.on('undo', function (data) {
            socket.broadcast.emit('undo', data);
        });

        socket.on('clear', function(data) {
            socket.broadcast.emit('clear', data);
        });

        socket.on('disconnect', function(){
            console.log('disconnected');
        });
    });

    return whiteboard;

};