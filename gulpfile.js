var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var plumber = require('gulp-plumber');
var minifyCss = require('gulp-minify-css');
var mainBowerFiles = require('main-bower-files');
var gulpFilter = require('gulp-filter');



gulp.task('js', function () {
    return gulp.src(['client/src/js/*.module.js'
        ,'client/src/js/**/*.module.js'
        , 'client/src/js/**/*.js'])
        .pipe(plumber())
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(plumber.stop())
        .pipe(gulp.dest('public/javascripts/'))
});

gulp.task('js-no-min', function() {
    return gulp.src(['client/src/js/*.module.js'
        ,'client/src/js/**/*.module.js'
        , 'client/src/js/**/*.js'])
        .pipe(plumber())
        .pipe(concat('app.min.js'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('test/'))
});

gulp.task('less', function(){
   return gulp.src(['client/src/css/main.less'])
       .pipe(plumber())
       .pipe(less())
       .pipe(minifyCss({compatibility: 'ie8'}))
       .pipe(plumber.stop())
       .pipe(gulp.dest('public/stylesheets/'))
});

gulp.task('watch', function(){
    gulp.watch('client/src/css/*.less', ['less']);
    gulp.watch(['client/src/js/*.module.js'
        ,'client/src/js/**/*.module.js'
        , 'client/src/js/**/*.js'], ['js']);
    gulp.watch(['bower_components/', 'bower_components/**/*.*'], ['bower'])
});

gulp.task('bower', function () {
    var jsFilter = gulpFilter('*.js');
    return gulp.src(mainBowerFiles())
        .pipe(jsFilter)
        .pipe(gulp.dest('public/javascripts/libraries/'));

});

gulp.task('default', ['less', 'js', 'bower']);

