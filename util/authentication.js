/**
 * Created by Tanatorn on 15-07-25.
 */

var app = require('../app')
var bcrypt = require('bcryptjs')
var ObjectId = require('mongodb').ObjectID
var jwt = require('jsonwebtoken')
var config = require('./config')

var authHelper = {};


/**
 * Checks if user is currently authorized to do this request
 */
authHelper.checkAuthenticatedUser = function (req, res, next) {
    var token = req.get('Authorization');
    var decode;
    try {
        decode = jwt.verify(token, config.secret, {ignoreExpiration: true});
        if (decode.exp > (Date.now() / 1000) ){
            next();
        } else {
            if(decode.id) {
                var id = new ObjectId(decode.id)
                var users = res.db.collection('users')
                users.findOne({ _id: id}, function (err, user) {
                    if (user != null) {
                        var data = {}
                        data.username = user.username
                        data.id = user._id
                        var token = jwt.sign(data, config.secret, {expiresInMinutes: 60});
                    }
                        res.sendStatus(401);
                })
            }
            return;
        }


    } catch (err) {
        console.log(err);
        res.sendStatus(401);
        return;
    }

};

/**
 * Checks if user is currently logged in
 */

authHelper.isLoggedIn = function (req, res, next) {
    if (!req.cookies.session) {
        req.isLoggedIn = false;
        next();
        return;
    }
    var token = JSON.parse(req.cookies.session).token;
    try {
        var user = jwt.verify(token, config.secret);
        req.isLoggedIn = true;
    } catch (err) {
        req.isLoggedIn = false;
    }
    next();
};

/**
 * Generate salt to hash password
 * @return {string} randomly generated salt
 */
authHelper.generateSalt = function () {
    return bcrypt.genSaltSync(10);
};

/**
 * Hashes the password with the salt
 * @param  {string} password
 * @param  {string} salt
 * @return {string} salted password
 */

authHelper.encryptPassword = function (password, salt) {
    return bcrypt.hashSync(password, salt);
};


/**
 * Generate a 16 bit salt
 * @return {string} token
 */
authHelper.generateToken = function () {
    return bcrypt.genSaltSync(16);
};
/**
 * @param  {string} password user-input
 * @param  {string} hash hashed password from db
 * @return {boolean}
 */
authHelper.checkPassword = function (password, hash) {
    return bcrypt.compareSync(password, hash);
}


module.exports = authHelper;