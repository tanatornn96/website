var express = require('express');
var router = express.Router();
var authHelper = require('../util/authentication');
var config = require('../util/config');

var jwt = require('jsonwebtoken')


/**
 * GET home page. 
 */
router.get('/', function(req, res, next) {
  res.render('index');
});


/**
 * Renders the specified partial
 * @param  {string} page
 */
router.get('/partials/:page', function (req, res) {
  var page = req.params.page;
  res.render('partials/' + page);
});


/**
 * Renders the specified partial in sub directory
 * @param {string} dir
 * @param  {string} page
 */
router.get('/partials/:dir/:page' ,authHelper.isLoggedIn, function ( req , res, next ){
    var path = req.params.dir + "/" + req.params.page;

    if ( path === 'blog/edit' ) {
        path = 'blog/create'
    }
    console.log(path);

    res.render('partials/' + path);

});


router.get('/api', function ( req , res ){
   res.send("I'm awake");
});


/**
 * @param  {username : string, password : string}
 * @return {user}
 */
router.post('/secret/register', function ( req , res ) {
    var db = res.db;
    var users = db.collection('users');
    var data = req.body;
    if (req.get('Content-Type').toLowerCase() == "application/json") {
        var username = data.username;
        var password = data.password;
        var salt = authHelper.generateSalt();
        var encryptedPassword = authHelper.encryptPassword(password, salt);
        var user = {
            username : username,
            password : encryptedPassword,
            salt : salt,
        };
        users.save(user, function (err, docs){
            if (err) {
                res.sendStatus(500);
                console.log(err);
            }
            res.json(docs);
        })
    } else {
        res.sendStatus(400);
    }


});

/**
 * @param  {username : string, password : string}
 * @return {user}
 */
router.post('/login', function ( req , res ) {
    var db = res.db;
    var users = db.collection('users');
    var data = req.body;
    if (req.get('Content-Type').toLowerCase() == "application/json") {
        var username = data.username;
        var password = data.password;
        users.findOne({ username : username}, function ( err , user ){
            if (user != null && authHelper.encryptPassword(password, user.salt) === user.password) {
                var data = {}
                data.username = user.username
                data.id = user._id
                var token = jwt.sign(data, config.secret, { expiresInMinutes: 60 });
                res.json({token : token, id : user._id});
            } else {
                res.sendStatus(401);
            }
        })

    } else {
        res.sendStatus(400);
    }

});


/**
 * Delete authorization token when logging out
 * @param  {string} username
 */
router.delete('/login/:id', function ( req, res ) {
    var authorization = req.get('Authorization');
    if (authorization != null) {
        res.sendStatus(200);
    } else {
        res.sendStatus(401);
    }
});


/**
 * Redirects other page to proper urls
 * @param {string} page
 */

router.get('/:page', function (req, res) {
    var page = req.params.page;
    res.redirect('/#/' + page);
});


/**
 * Redirects other page to proper subdir urls
 * @param {string} subdir
 * @param {string} page
 */

router.get('/:subdir/:page', function (req, res, next) {
    var subdir = req.params.subdir;
    var page = req.params.page;
    if( subdir === 'api') {
        next()
        return
    }
    res.redirect('/#/' + subdir + '/' + page);
});


/**
 * Handle Specific Blog Pages
 * @param  {string} id
 */

router.get('/blog/detail/:id', function (req, res) {
    var id = req.params.id;
    res.redirect('/#/blog/detail/' + id);
});

/**
 * Handle specific blog edits
 * @param {string} id
 */

router.get('/blog/edit/:id', function (req, res) {
    var id = req.params.id;
    res.redirect("/#/blog/edit/" + id)
})



module.exports = router;
