/**
 * Created by Tanatorn on 15-07-24.
 * This is the /api/blog/ endpoint, only include blog endpoints in this file
 */

var express = require('express');
var router = express.Router();
var authHelper = require('../../util/authentication');
var path = require('path');
var request = require('request');
var multer = require('multer');
var fs = require('fs');
var mime = require('mime');
var ObjectId = require('mongodb').ObjectID;


/**
 * @return {blog[]}
 */
    router.get("/", function(req, res){
    var db = res.db;
    var blogPosts = db.collection('blogposts');
    blogPosts.find({}).sort({ date : -1 }, function (err , docs) {
        if (err) {
            res.sendStatus(500);
            console.log(err);
        }
        res.status(200).json(docs);
    })

});

// Post a blog
router.post("/", authHelper.checkAuthenticatedUser, function (req , res) {
    var db = res.db;
    var data = req.body;
    data.date = new Date(data.date);
    var blogPost = db.collection('blogposts');
    blogPost.save(data, function (err , doc) {
        if (err) {
            res.sendStatus(401);
            console.log(err);
        }
        res.status(200).json(doc);

    });

});


/**
 * Upload Image to Server
 * @return {contentType : string, data : string, exists : bool } data is the path to the image
 */
router.post("/image",
    authHelper.checkAuthenticatedUser,
    multer({
        dest: 'public/images/blog/uploads/' ,
        limits: {fileSize: 500000}
    }).single('file'),
    function (req, res) {
        var path = req.file.path;
        var fileExtension = mime.extension(req.file.mimetype);
        var newPath = path + "." + fileExtension;
        fs.renameSync(path, newPath);
        var data = {
            contentType : req.file.mimetype,
            data : newPath,
            exists : true
        };
        res.status(201).json(data);

    });


/** 
 * Edit post
 * @param {string} id ObjectID of the post
 */
router.put("/post/:id", authHelper.checkAuthenticatedUser, function (req , res) {
    var id = new ObjectId(req.params.id);
    var db = res.db;
    var data = req.body;
    data.date = new Date(data.date);
    var blogPosts = db.collection('blogposts');
    blogPosts.findAndModify({
        query : { _id : id },
        update : { $set : data },
        new : true
    }, function (err, doc) {
        if (err)
            res.sendStatus(500);
        if (doc)
            res.sendStatus(200);
        else
            res.sendStatus(404);

    });
});

/** 
 * Delete post
 * @param {string} id ObjectID of the post
 */
router.delete("/post/:id", authHelper.checkAuthenticatedUser, function (req, res) {
    var id = new ObjectId(req.params.id);
    var db = res.db;
    var data = req.body;
    var blogPosts = db.collection('blogposts');
    blogPosts.remove({ _id : id}, function (err, doc) {
        if (err)
            res.sendStatus(500);
        else if (doc)
            res.status(200).send(doc);
        else
            res.sendStatus(404);
        console.log(doc);
    })
});



// Get latest page
router.get( "/page" , function ( req, res ) {
    var db = res.db;
    var blogPosts = db.collection('blogposts');
    blogPosts.find({}).sort({ date : -1 }, function (err, docs) {
        if (err) {
            res.sendStatus(500);
            console.log(err);
        }
        var page = [];
        var startingIndex = 0;
        if (startingIndex < docs.length + 1) {
            for (var i = 0; i < 5; i++) {
                if(typeof docs[i+ startingIndex] !== "undefined")
                    page.push(docs[i + startingIndex]);

            }
        }
        var data = {
            posts : page,
            pages : Math.ceil(docs.length / 5)
        };
        res.status(200).json(data);
    })
});

/** 
 * Get by page number
 * @param {int} number page number
 */
router.get( "/page/:number", function( req , res ) {
    // Page Number is 1-indexed
    var pageNumber = req.params.number - 1;
    var db = res.db;
    var blogPosts = db.collection('blogposts');
    blogPosts.find({}).sort({date : -1 }, function (err, docs){

        if (err) {
            res.sendStatus(500);
            console.log(err);
        }
        var page = [];
        var startingIndex = pageNumber * 5;
        if (startingIndex < docs.length + 1) {
            for (var i = 0; i < 5; i++) {
                if(typeof docs[i+ startingIndex] !== "undefined")
                    page.push(docs[i + startingIndex]);

            }
        }
        var data = {
            posts : page,
            pages : Math.ceil(docs.length / 5)
        };
        res.status(200).json(data);
    })

});


//Get Specific Page
router.get("/post/:id", function (req, res ) {
    //Parse ID into ObjectID using Mongo Native Driver
    var id = new ObjectId(req.params.id);
    var db = res.db;
    var blogPosts = db.collection('blogposts');
    blogPosts.findOne({ _id : id }, function (err , post) {
        if (err) {
            console.log(err)
            res.sendStatus(500)
        }
        res.send(post);
    });

});

/** 
 * Post a comment
 * @param {string} id Individual post's ObjectID
 */
router.post("/post/:id", checkCommentParams, function (req , res) {
    var id = new ObjectId(req.params.id);
    var db = res.db;
    var data = req.body;
    var blogPosts = db.collection('blogposts');
    var fileDir = rootDir + "/public/images/blog/user/";
    var filePath = rootDir + "/public/images/blog/user/" + data.user.id + '.png';
    request(data.user.profile, {encoding : null},  function (err, response, body) {
        if (err)
            console.log(err)

        //If file directory doesn't exist, make it
        if (!fs.existsSync(fileDir)){
            fs.mkdirSync(fileDir);
        }

        fs.writeFile(filePath, body, function (err) {
            if (err)
                console.log(err)
            data.user.profile = filePath;
            blogPosts.findAndModify({
                query : { _id : id },
                update : { $push : { comments : data }},
                new : true
            }, function (err, doc) {
                if (err)
                    res.sendStatus(500);
                if (doc)
                    res.status(200).json(doc);
                else
                    res.sendStatus(404)
            });

        })

    });
});

/**
 * Middleware that checks comment parameters
 */

function checkCommentParams ( req, res , next) {
    var data = req.body;
    var comment = data.comment;

    if (comment && comment.length > 0 && comment.length < 201) {
        console.log("Valid Request");
        next();
    } else {
        console.log("Request not valid");
        res.sendStatus(400);
    }

}

/**
 * Get All Tags
 * @return {tag[]} 
 */
router.get("/tag", function ( req , res ) {
    var db = res.db;
    var blogPosts = db.collection('blogposts');
    var tags = [];
    blogPosts.find({}, function ( err, posts ) {
        if (err) {
            res.sendStatus(500);
            console.log(err);
        }
        posts.forEach( function (post) {
            post.tags.forEach(function (tag) {
                if (tags.indexOf(tag) == -1)
                    tags.push(tag);
            })
        });
        var data = { tags : tags};
        res.status(200).json(data);
    })
});

/**
 * @param  {string} tag desired tag
 * @return {blog[]}
 */
router.get("/tag/:tag", function (req, res) {
    var db = res.db;
    var blogPosts = db.collection('blogposts');
    var tag = req.params.tag;
    blogPosts.find({tags : tag}).sort({date : -1 }, function (err, docs) {
        if(err) {
            res.sendStatus(500);
            console.log(err);
        }
        res.status(200).json(docs);
    })

});




module.exports = router;