/**
 * Created by Tanatorn on 15-07-25.
 */
angular
    .module('app')
    .service('authService', authService);

authService.$inject = ['$cookies', '$http', '$state', 'ModalFactory'];

function authService($cookies, $http, $state, ModalFactory) {
    this.login = function(data) {
        var req = {
            method: "POST",
            url: "/login",
            headers: {
                'Content-Type': "application/json"
            },
            data: data
        };

        $http(req).success(function(data) {
            $cookies.put('session', JSON.stringify(data));
            $state.go('blog.index');
        }).error(function(data, status) {
            if (status == 401) {
                var invalidPasswordModal = new ModalFactory(null);
                invalidPasswordModal.setTitle("Invalid Credentials");
                invalidPasswordModal.setDescription("You have entered an invalid username/password, please try again");
                invalidPasswordModal.show();
            }
        });

    };
    this.logout = function(state) {
        var token = null;
        var id = null;
        var cookieString = $cookies.get('session');
        if (cookieString) {
            var cookie = JSON.parse(cookieString);
            token = cookie.token;
            id = cookie._id;
        }
        if (token) {
            var req = {
                method: "DELETE",
                url: "/login/" + id,
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': token
                }
            };
            $http(req).success(function(data) {
                $cookies.remove('session');
                $state.go(state, {}, {
                    reload: true
                });

            }).error(function(data, status) {
                if (status == 401) {
                    $state.go(state)
                }
            })
        } else {
            var invalidTokenModal = new ModalFactory(MODAL_TYPE_UNAUTHORIZED);
            invalidTokenModal.show();
        }
    };

    this.getToken = function() {
        var cookieString = $cookies.get('session');
        if (cookieString) {
            return JSON.parse(cookieString).token;
        }
        return null;
    }
}