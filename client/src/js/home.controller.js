angular
    .module('app')
    .controller('homeController', HomeController);

HomeController.$inject = ['$window', 'ModalFactory'];
function HomeController($window, ModalFactory){
    var home = this;

    home.Resume = function () {
        $window.open('/docs/resume.pdf');
    }
}

