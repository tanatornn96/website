angular
	.module('app.core')
	.service('loadingService', LoadingService);

LoadingService.$inject = ['$rootScope'];
function LoadingService($rootScope) {
	this.isRequestProcessing = function( isProcessing ) {
		$rootScope.loading = isProcessing;
	};


}