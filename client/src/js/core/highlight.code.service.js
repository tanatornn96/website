/**
 * Created by Tanatorn on 15-09-01.
 */
angular
    .module('app')
    .service('HighlightCodeService', HighlightCodeService);

function HighlightCodeService() {

    /**
     * Polls until highlight code is done
     * @param scope Scope of the controller using ControllerAs Syntax
     */
    this.run = function pollHighlightCode( scope ) {
        scope.isCodeHighlighted = false;
        var polling = setInterval(function () {
            $('pre code').each(function (i, block) {
                clearInterval(polling);
                hljs.highlightBlock(block);
                scope.isCodeHighlighted = true;
            });
        }, 500);
    }
}