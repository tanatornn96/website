angular
    .module('app')
    .directive('ngActive', active);
active.$inject = ['$timeout'];

function active($timeout) {
    return {

        restrict: 'AE',


        link: function($scope, elem, attrs) {
            $timeout(function() {
                var list = [];
                for (var i = 0; i < elem.children().length; i++) {
                    list.push(angular.element(elem.children()[i]));
                }

                list.forEach(function(icon, index) {
                    if (icon.attr('id') !== "expander") {
                        icon.click(function() {
                            icon.addClass("active");
                            list.forEach(function(otherIcon) {
                                if (icon != otherIcon)
                                    otherIcon.removeClass("active");
                            })
                        });
                    }
                });


                if (typeof $scope.ngActiveList === 'undefined') {
                    $scope.ngActiveList = [];
                    $scope.ngActiveParent = [];
                }
                $scope.ngActiveList.push(list);
                $scope.ngActiveParent.push(angular.element(elem));

                $scope.enablePage = function (parents, lists, parentId, elementId) {
                    for (var i = 0; i < parents.length; i++) {
                        if (parents[i].attr('id') === parentId) {
                            lists[i].forEach(function(icon) {
                                if(elementId == icon.attr('id')) {
                                    icon.addClass("active")
                                } else {
                                    icon.removeClass("active");
                                }
                            });
                        }
                    }
                };

                $scope.disableAll = function(parents, lists, id) {
                    for (var i = 0; i < parents.length; i++) {
                        if (parents[i].attr('id') === id) {
                            lists[i].forEach(function(icon, index) {
                                icon.removeClass("active");
                            });
                        }
                    }

                };

            })


        }
    };
}