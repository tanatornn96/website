angular
    .module('app')
    .factory('ModalFactory', ModalFactory);

ModalFactory.$inject = ['$rootScope']

/**
 * Creates a modal
 * @param { title : string, description : string} values to initialize modal
 */
function ModalFactory($rootScope) {
    return function(type) {
        if (type) {
            switch (type) {
                case MODAL_TYPE_UNSPECIFIED_ERROR:
                    this.title = "Oops! Something went wrong.";
                    this.description = "An unexpected error has occured, please try again";
                    break;
                case MODAL_TYPE_UNAUTHORIZED:
                    this.title = "You are unauthorized.";
                    this.description = "Please login and try again.";
                    break;
            }
        }

        this.setTitle = function(title) {
            this.title = title;
        }

        this.setDescription = function(description) {
            this.description = description;
        }
        this.setButtons = function(buttons) {
            this.buttons = buttons;
        }

        this.show = function() {
            //Default button is just a dismiss button
            if (!this.buttons) {
                this.buttons = {
                    cancel: {
                        label: "Dismiss",
                        className: "btn-default"
                    }
                }
            }
            if (this.description && this.title) {
                bootbox.dialog({
                    message: this.description,
                    title: this.title,
                    buttons: this.buttons
                });
            } else {
                bootbox.alert("An unexpected error has occured");
            }
        }
    }
}