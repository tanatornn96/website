/**
 * Created by Tanatorn on 15-07-26.
 */

angular
    .module('app')
    .filter('range', rangeFilter);

function rangeFilter() {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 1; i <= total; i++ ){
            input.push(i);
        }
        return input;
    }
}