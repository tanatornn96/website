/**
 * Created by Tanatorn on 8/17/2015.
 */
angular
    .module('app')
    .directive('linebreakParser', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, controller) {
            controller.$parsers.push(function(value) {
                return value.replace(/(\r\n|\n|\r)/g, '  ');
            });
        }
    }
});