/**
 * Created by Tanatorn on 15-08-12.
 */

angular
    .module('app')
    .filter('unsafe', unsafeHTML);

unsafeHTML.$inject = ['$sce'];

function unsafeHTML($sce) {
    return function(val) {
    	return $sce.trustAsHtml(val);
    };
}