angular
    .module('app')
    .config(config)
    .run(onStateChange);

config.$inject = ['$stateProvider' , '$urlRouterProvider', '$locationProvider'];
onStateChange.$inject= ['$rootScope', '$window', 'loadingService', '$location', 'navigationService'];
function config( $stateProvider, $urlRouterProvider, $locationProvider ){

    $urlRouterProvider.otherwise("/");


    //Get rid of '/#/' for routes
    $locationProvider.html5Mode(true);

    $stateProvider
        .state('home', home )
        .state('project', project)
        .state('about', about);

}
var home = {
    url:"/",
    templateUrl: "partials/home",
    controller : 'homeController',
    controllerAs: 'home'
};

var project = {
	url:"/project",
	templateUrl : "partials/project",
    controller : "projectController",
    controllerAs : 'project'
};

var about = {
	url: "/about",
	templateUrl: "partials/about"
};



function onStateChange($rootScope, $window, loadingService, $location, navigationService){
    $rootScope.$on('$viewContentLoaded', function(event, viewConfig) {
        $window.scrollTo(0,0);
        navigationService.setActive($location.path());
    });
    $rootScope.$on('$stateChangeStart', function (event, viewConfig) {
        loadingService.isRequestProcessing(true);

    });
    $rootScope.$on('$stateChangeSuccess', function (event, viewConfig) {
        loadingService.isRequestProcessing(false);
    });

}