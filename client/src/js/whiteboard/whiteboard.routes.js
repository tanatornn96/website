/**
 * Created by Tanatorn on 15-07-31.
 */
angular
    .module('app.whiteboard')
    .config(whiteboardRoutes);

whiteboardRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

function whiteboardRoutes( $stateProvider, $urlRouterProvider ){

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('whiteboard', whiteboard)
}

var whiteboard = {
        url: "/whiteboard",
        templateUrl : "partials/whiteboard/index",
        controller : "WhiteboardController",
        controllerAs: "whiteboard"
};
