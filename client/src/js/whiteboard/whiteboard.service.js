angular
	.module('app.whiteboard')
	.service('whiteboardService', WhiteboardService);

WhiteboardService.$inject = [ '$rootScope'];
function WhiteboardService ($rootScope) {
	this.disconnect = function () {
		if($rootScope.io) {
            $rootScope.io.disconnect();
            $rootScope.io = null;

        }
	};

	this.connect = function () {
		$rootScope.io = io.connect('http://tnantawinyoo.com/whiteboard',{ forceNew : true});
		return $rootScope.io;
	}
}