/**
 * Created by Tanatorn on 15-07-31.
 */
angular
    .module('app.whiteboard')
    .controller('WhiteboardController', WhiteboardController);

WhiteboardController.$inject = ['$interval', 'whiteboardService', '$scope'];
function WhiteboardController($interval, whiteboardService, $scope){
    var vm = this;
    var TYPE_MOUSE_DOWN = "mousedown";
    var TYPE_MOUSE_UP = "mouseup";
    var TYPE_MOUSE_MOVE = "mousemove";
    

    vm.isDrawing = false;
    //history of actions
    vm.history = [];
    vm.lastAction = [];
    vm.draw = function(e) {
        vm.current = {
            x : e.offsetX,
            y : e.offsetY,
            tool : vm.tool
        };
        if(e.type === TYPE_MOUSE_DOWN) {
            vm.isDrawing = true;
            vm.lastAction = [];
            vm.whiteboard.strokeStyle = vm.tool.color;
            vm.whiteboard.lineWidth = vm.tool.size;
            vm.lastAction.push(vm.current);

            draw(vm.current, vm.current, false);
        }

        if(e.type === TYPE_MOUSE_MOVE && vm.isDrawing) {
            draw(vm.prev, vm.current, false);
            vm.lastAction.push(vm.current);
        }
        if(e.type === TYPE_MOUSE_UP) {
            vm.isDrawing = false;
            vm.lastAction.push(vm.current);
            vm.history.push(vm.lastAction);
            vm.io.emit('pencilUp', vm.lastAction);
        }
    };

    function draw(prev, next, isRemote) {
        vm.whiteboard.beginPath();
        vm.whiteboard.moveTo(prev.x, prev.y);
        vm.whiteboard.lineTo(next.x, next.y);
        vm.whiteboard.strokeStyle = next.tool.color;
        vm.whiteboard.lineWidth = next.tool.size;
        vm.whiteboard.lineJoin = "round";
        vm.whiteboard.stroke();
        vm.whiteboard.closePath();
        vm.prev = next;
        if(!isRemote)
            vm.io.emit('pencilDown', vm.lastAction);
    }

    function drawIO(prev, next) {
        vm.whiteboard.beginPath();
        vm.whiteboard.moveTo(prev.x, prev.y);
        vm.whiteboard.lineTo(next.x, next.y);
        vm.whiteboard.strokeStyle = next.tool.color;
        vm.whiteboard.lineWidth = next.tool.size;
        vm.whiteboard.lineJoin = "round";
        vm.whiteboard.stroke();
        vm.whiteboard.closePath();
    }

    function redraw(history, isRemote) {
        var whiteboard = $("#whiteboard")[0];
        vm.whiteboard.clearRect(0,0,whiteboard.clientWidth, whiteboard.clientHeight);
        if(history.length > 0) {
            for (var j = 0; j < history.length; j++) {
                if(history[j] == true){
                    vm.whiteboard.clearRect(0,0,whiteboard.clientWidth, whiteboard.clientHeight);
                } else {
                    var point = history[j];
                    for (var i = 1; i < point.length; i++) {
                        draw(point[i - 1], point[i], isRemote);
                    }
                }
            }
        }
    }



    vm.init = function() {
        //Configure Canvas
        var whiteboard = $("#whiteboard")[0];
        vm.whiteboard = whiteboard.getContext("2d");
        vm.whiteboard.strokeStyle = "#000000";
        vm.toolSelect('pencil');
        vm.whiteboard.canvas.width = whiteboard.clientWidth;
        vm.whiteboard.canvas.height = whiteboard.clientHeight;

        //Listen for page change
        $(window).on('resize', function() {
            vm.whiteboard.canvas.width = whiteboard.clientWidth;
            vm.whiteboard.canvas.height = whiteboard.clientHeight;
            redraw(vm.history, false);
        });

        //Connect to WebSocket
        vm.io = whiteboardService.connect();


        //Listen for pencil down then draws
        vm.io.on('pencilDown', function (data) {
            for (var i = 1; i < data.length; i++) {
                drawIO(data[i-1], data[i]);
            }

        });

        //Listen for pencil up, pushes the collective action into history
        vm.io.on('pencilUp', function (data) {
            vm.history.push(data);
        });

        //Listen for undo, recieves whole canvas data and redraws
        vm.io.on('undo', function(data) {
            vm.history = data;
            redraw(vm.history, true);
        });

        vm.io.on('clear', function(data) {
            var whiteboard = $("#whiteboard")[0];
            vm.whiteboard.clearRect(0,0,whiteboard.width,whiteboard.height);
            vm.history.push(true);
        });

    };

    vm.undo = function () {
        if(vm.history.length > 0) {
            vm.history.pop();
            redraw(vm.history, true);
            vm.io.emit('undo', vm.history);
        }
    };

    vm.clear = function () {
        var whiteboard = $("#whiteboard")[0];
        var confirming = confirm("Are you sure you want to clear the whiteboard?");
        if(confirming) {
            vm.whiteboard.clearRect(0, 0, whiteboard.width, whiteboard.height);
            vm.history.push(true);
            vm.io.emit('clear', true);
        }

    };

    vm.toolSelect = function( tool ) {
        switch (tool) {
            case 'pencil':
                vm.tool = {
                    color: "black",
                    size : 2
                };
                break;
            case 'eraser':
                vm.tool =  {
                    color : "white",
                    size : 5
                };
                break;
        }
    }

    $scope.$on('$destroy', function () {
        whiteboardService.disconnect();
    })





}
