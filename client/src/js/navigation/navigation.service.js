/**
 * Created by Tanatorn on 15-08-08.
 */
angular
    .module('app.navigation')
    .service('navigationService', navigationService);

function navigationService () {
    this.setActive = function (url) {
        //Matches base url and sets the state of the icon accordingly
        var regex = /\/(.*)/m;
        var m = regex.exec(url)[1];
        var state = m.substring(0,(m.indexOf('/') > -1 ? m.indexOf('/') : m.length));
        if (state === "")
            state = "home";
        var id =  state + '-item';

        $('#' + id).addClass('active');
    };
}
