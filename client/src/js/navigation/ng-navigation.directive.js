angular
    .module('app')
    .directive('ngNavigation',navbar);

function navbar(){
    return {
        restrict: 'A',

        link: function (scope, elem, attrs) {
            var container = angular.element(elem);

            //Expander
            var expander = angular.element(elem.children()[0]);
            $('#expander').click(function(){
                if(expander.hasClass("expanded")){
                    container.animate({
                        width: '60px'
                    });
                    expander.animate({
                        width:'60px'
                    });
                    expander.toggleClass("expanded")
                    container.toggleClass("expanded");
                } else {
                    container.animate({
                        width: '200px'
                    });
                    expander.animate({
                        width:'200px'
                    }, function(){
                        expander.toggleClass("expanded")
                        container.toggleClass("expanded");
                    });
                }
            })



        }
    };
}