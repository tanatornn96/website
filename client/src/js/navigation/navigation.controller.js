angular
    .module('app')
    .controller('NavigationController', NavigationController);

NavigationController.$inject = ['$window'];
function NavigationController($window){
    var nav = this;
    nav.resume = function() {
        $window.open('/docs/resume.pdf');
    };



}
