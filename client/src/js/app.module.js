/**
 * Created by Tanatorn on 15-07-17.
 */

angular
    .module('app', [
        'app.blog',
        'app.navigation',
        'app.whiteboard'
    ]);

angular
    .module('app.core', ['ui.router','ngCookies']);

var MODAL_TYPE_UNSPECIFIED_ERROR = 1;
var MODAL_TYPE_UNAUTHORIZED = 2;