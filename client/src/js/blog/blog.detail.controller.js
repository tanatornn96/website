angular
    .module('app.blog')
    .controller('blogDetailController', BlogDetailController);

BlogDetailController.$inject = ['post', 'HighlightCodeService'];

function BlogDetailController(post, HighlightCodeService) {
    var vm = this;

    vm.post = post;

    vm.init = function () {
        HighlightCodeService.run(vm);
    };





}