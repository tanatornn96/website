/**
 * Created by Tanatorn on 15-07-25.
 */
angular
    .module('app.blog')
    .controller('loginController', loginController);

loginController.$inject = ['authService'];

function loginController(authService) {
    var login = this;
    login.username = "";
    login.password = "";
    login.submit = function () {
        var data = {
            username : login.username,
            password : login.password
        };
        authService.login(data);
    }
}