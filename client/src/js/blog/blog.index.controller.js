/**
 * Created by Tanatorn on 15-07-24.
 */
angular
    .module('app.blog')
    .controller('blogController', BlogController);

BlogController.$inject = ['isLoggedIn', 'blogPosts', 'tags', 'authService',
    'blogService', '$window', '$state', '$scope', 'ModalFactory','HighlightCodeService'
];

function BlogController(isLoggedIn, blogPosts, tags, authService,
                        blogService, $window, $state, $scope, ModalFactory,HighlightCodeService) {
    var blog = this;

    //Initialize Variables on load
    blog.isLoggedIn = isLoggedIn;
    blog.tags = tags;
    blog.posts = blogPosts.posts;
    blog.pageCount = blogPosts.pages;
    blog.currentPage = 1;

    blog.init = function () {
        HighlightCodeService.run(blog);
    };

    blog.getPage = function (id, reload) {

        if (id.page != blog.currentPage || reload) {

            blogService.getPosts(id.page).then(function (res) {

                //Refresh posts and page indicator
                blog.posts = res.posts;
                blog.currentPage = id.page;

                //Scroll to top and disable all tags
                $window.scrollTo(0, 0);
                $scope.disableAll($scope.ngActiveParent, $scope.ngActiveList, 'tag-list');
                HighlightCodeService.run(blog);

            });

        }

    };

    blog.createPost = function () {
        $state.go('blog.create');
    };

    blog.buttonLabel = isLoggedIn ? "Logout" : "Login";

    blog.buttonAction = function () {
        if (isLoggedIn)
            authService.logout('blog.index');
        else
            $state.go('login');
    };

    blog.getPostsByTag = function (tag) {
        blogService.getPostsByTag(tag).then(function (blogposts) {

            blog.posts = blogposts;
            blog.currentPage = -1;

            HighlightCodeService.run(blog);
            $scope.disableAll($scope.ngActiveParent, $scope.ngActiveList, 'page-list');


        });
    };

    blog.delete = function (e) {
        var deleteModal = new ModalFactory(null);
        deleteModal.setTitle("Delete this post");
        deleteModal.setDescription("Are you sure you want to delete this post?");
        var buttons = {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            success: {
                label: "Delete",
                className: "btn-primary",
                callback: function () {
                    var id = e.post._id;
                    blogService.deletePost(id).then(function (success) {
                        if (success) {
                            var id = {
                                page: blog.currentPage
                            };
                            blog.getPage(id, true)
                        }
                    });
                }
            }
        }
        deleteModal.setButtons(buttons);
        deleteModal.show();


    }




}