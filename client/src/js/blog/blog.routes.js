angular
    .module('app.blog')
    .config(blogRoutes);

blogRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

function blogRoutes($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when("/blog", "/blog/");


    $stateProvider
        .state('blog', blog)
        .state('blog.index', blogIndex)
        .state('blog.detail', blogDetail)
        .state('blog.create', blogCreate)
        .state('blog.edit', blogEdit)
        .state('login', login);

}

var blog = {
    url: "/blog",
    abstract: true,
    templateUrl: 'partials/blog/base',
    redirectTo: 'blog.index'

};

var blogIndex = {
    url: "/",
    views: {
        'main': {
            templateUrl: "partials/blog/index",
            controller: "blogController",
            controllerAs: "blog",
            resolve: {
                isLoggedIn: ['$cookies', function ($cookies) {
                    return $cookies.get('session') != null;
                }],
                blogPosts: ['blogService', function (blogService) {
                    return blogService.getPosts(1);
                }],
                tags: ['blogService', function (blogService) {
                    return blogService.getTags();
                }]
            }
        }
    }

};

var blogCreate = {
    url: "/create",
    views: {
        'main': {
            templateUrl: "partials/blog/create",
            controller: "blogCreateController",
            controllerAs: "blog"
        }
    }

};

var login = {
    url: "/login",
    templateUrl: "partials/blog/login",
    controller: "loginController",
    controllerAs: "login"

};

var blogDetail = {
    url: "/detail/:id",
    views: {
        'main': {
            templateUrl: "partials/blog/detail",
            resolve: {
                post: ['$stateParams', 'blogService', function ($stateParams, blogService) {
                    return blogService.getPost($stateParams.id);
                }]
            },
            controller: "blogDetailController",
            controllerAs: "blogDetail"
        }
    }

};

var blogEdit = {
    url: "/edit/:id",
    views: {
        'main': {
            templateUrl: "partials/blog/create",
            controller: "blogEditController",
            controllerAs: "blog",
            resolve: {
                post: ['$stateParams', 'blogService', function ($stateParams, blogService) {
                    return blogService.getPost($stateParams.id);
                }]
            }
        }
    }

};

