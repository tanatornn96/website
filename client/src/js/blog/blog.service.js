/**
 * Created by Tanatorn on 15-07-25.
 */
angular
    .module('app.core')
    .service('blogService', blogService);

blogService.$inject = ['$cookies', '$http', '$state', '$q', 'ModalFactory', 'loadingService'];

function blogService($cookies, $http, $state, $q, ModalFactory, loadingService) {

    /** Endpoints that creates and modify posts */
    this.createPost = function(data) {
        var token = null;
        var cookieString = $cookies.get('session');
        if (cookieString) {
            var cookie = JSON.parse(cookieString);
            token = cookie.token;
        }
        if (token) {
            var req = {
                method: "POST",
                url: "/api/blog",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': token
                },
                data: data
            };
            $http(req).success(function(data, status) {
                $state.go('blog.index');
            }).error(function(data, status) {
                if (status == 401)
                    $state.go('login')
            })
        } else {
            var invalidTokenModal = new ModalFactory(MODAL_TYPE_UNAUTHORIZED);
            invalidTokenModal.show();
        }
    };

    this.editPost = function(post) {
        var id = post.id;
        var token = null;
        var cookieString = $cookies.get('session');
        if (cookieString) {
            var cookie = JSON.parse(cookieString);
            token = cookie.token;
        }

        if (token) {
            var req = {
                method: "put",
                url: "/api/blog/post/" + id,
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': token
                },
                data: post.data
            };

            $http(req).success(function(data, status) {
                $state.go('blog.index');
            }).error(function(data, status) {
                if (status == 401)
                    $state.go('login')
            })
        } else {
            var invalidTokenModal = new ModalFactory(MODAL_TYPE_UNAUTHORIZED);
            invalidTokenModal.show();
        }

    };

    this.deletePost = function(id) {
        loadingService.isRequestProcessing(true);
        var def = $q.defer();
        var token = null;
        var cookieString = $cookies.get('session');
        if (cookieString) {
            var cookie = JSON.parse(cookieString);
            token = cookie.token;
        }
        if (token) {
            var req = {
                method: "delete",
                url: "/api/blog/post/" + id,
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': token
                }
            };
            $http(req).success(function(data, status) {
                def.resolve(true);
                loadingService.isRequestProcessing(false);
            }).error(function(data, status) {
                def.reject("Error");
                if (status == 401)
                    $state.go('login')
                loadingService.isRequestProcessing(false);
            });
        } else {
            var invalidTokenModal = new ModalFactory(MODAL_TYPE_UNAUTHORIZED);
            invalidTokenModal.show();
            def.reject("Invalid Token");
            loadingService.isRequestProcessing(false);
        }
        return def.promise;
    };

    /** Endpoints that gets posts and attributes related to posts */

    this.getPosts = function(page) {
        var url = '/api/blog/page/' + page;
        var def = $q.defer();
        loadingService.isRequestProcessing(true);
        $http.get(url).success(function(data, status) {
            def.resolve(data);
            loadingService.isRequestProcessing(false);
        }).error(function(data, status) {
            def.reject("Error")
            loadingService.isRequestProcessing(false);
        });
        return def.promise;
    };

    this.getPostsByTag = function(tag) {
        var url = '/api/blog/tag/' + tag;
        var def = $q.defer();
        loadingService.isRequestProcessing(true);
        $http.get(url).success(function(data, status) {
            def.resolve(data);
            loadingService.isRequestProcessing(false);
        }).error(function(data, status) {
            def.reject("Error");
            loadingService.isRequestProcessing(false);
        });
        return def.promise;
    };

    this.getTags = function() {
        var url = 'api/blog/tag';
        var def = $q.defer();
        $http.get(url).success(function(data, status) {
            def.resolve(data.tags);
        }).error(function(data, status) {
            def.reject("Error");
        });
        return def.promise;
    };

    this.getPost = function(postId) {
        var url = 'api/blog/post/' + postId;
        var def = $q.defer();
        loadingService.isRequestProcessing(true);
        $http.get(url).success(function(data, status) {
            def.resolve(data);
            loadingService.isRequestProcessing(false);
        }).error(function(data, status) {
            def.reject("Error");
            loadingService.isRequestProcessing(false);
        });
        return def.promise;
    };

    /** Endpoints that encompasses comments */
    this.createComment = function(id, data) {
        var def = $q.defer();
        loadingService.isRequestProcessing(true);
        var url = 'api/blog/post/' + id;
        var req = {
            method: "post",
            url: url,
            headers: {
                'Content-Type': "application/json",
            },
            data: data
        };
        $http(req).success(function(data, status) {
            def.resolve(data);
            loadingService.isRequestProcessing(false);
        }).error(function(data, status) {
            def.reject("Error");
            loadingService.isRequestProcessing(false);
        });


        return def.promise;
    };




}