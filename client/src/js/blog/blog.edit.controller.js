
angular
    .module('app.blog')
    .controller('blogEditController', blogEditCtrl);

blogEditCtrl.$inject = ['$scope', 'authService', 'blogService', 'Upload', 'post'];

function blogEditCtrl ($scope, authService, blogService, Upload, post){
    var blogEdit = this;

    blogEdit.imageArray = [];
    blogEdit.init = function () {
        //Initialize the CKEDITOR and set the default value
        CKEDITOR.replace( 'editor' );
        CKEDITOR.instances.editor.setData(post.body);
    };



    //Watch file change on upload file
    blogEdit.fileWatcher = $scope.$watch( function() {
        // Return the "result" of the watch expression.
        return ( blogEdit.file );
    }, function (newValue) {
        if(typeof newValue !=  "undefined" && newValue != null)
            blogEdit.upload([blogEdit.file]);
    });

    blogEdit.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: 'api/blog/image',
                    file: file,
                    headers : {
                        'Authorization' : authService.getToken(),
                        'Content-Type': file.type

                    }
                }).progress(function (evt) {
                    blogEdit.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                }).success(function (data) {
                    blogEdit.imageArray.push(data);
                }).error(function (data, status) {
                    console.log('error status: ' + status);
                })
            }
        }
    };
    blogEdit._id = post._id;
    blogEdit.title = post.title;
    blogEdit.body = post.body  ;
    blogEdit.rawTags = processTags(post.tags);
    blogEdit.rawDate = processDateObject(post.date);
    blogEdit.image = post.image;
    blogEdit.summary = {};
    blogEdit.summary.length = post.summary;

    blogEdit.create = function () {
        var id = blogEdit._id;

        var data = {
            title : blogEdit.title,
            body : CKEDITOR.instances.editor.getData(),
            tags : processRawTags(blogEdit.rawTags),
            date : processRawDate(blogEdit.rawDate),
            image : blogEdit.image
        };

        if (blogEdit.summary) {
            data.summary = blogEdit.summary.length;
        }

        var post = {
            id : id,
            data : data
        };
        blogService.editPost(post);
    };

    function processRawTags(rawTags) {
        if(rawTags === "")
            return [];
        return rawTags.split(",");
    }

    function processRawDate(rawDate) {
        if (rawDate === "")
            return new Date();
        var inputArray = rawDate.split("-");
        return new Date(inputArray[0],inputArray[1] - 1, inputArray[2],0,0,0,0);


    }

    function processTags(tags) {
        return tags.join();
    }

    function processDateObject(isodate) {
        var date = new Date(isodate);
        var year = date.getFullYear();
        var month = (parseInt(date.getMonth()) + 1);
        var day = date.getDate();
        return year + "-" + month + "-" + day;
    }


    $scope.$on('$destroy', function() {
        blogEdit.fileWatcher();
    });


}