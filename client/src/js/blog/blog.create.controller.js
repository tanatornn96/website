/**
 * Created by Tanatorn on 15-07-25.
 */
angular
    .module('app.blog')
    .controller('blogCreateController', blogCreateCtrl);

blogCreateCtrl.$inject = ['$scope', 'authService', 'blogService', 'Upload'];

function blogCreateCtrl ($scope, authService, blogService, Upload){
    var blogCreate = this;

    blogCreate.imageArray = [];
    blogCreate.init = function () {
        //Initialize Editor
        CKEDITOR.replace( 'editor' );
    };


    //Watch file change on upload file
    blogCreate.fileWatcher = $scope.$watch( function() {
        // Return the "result" of the watch expression.
        return ( blogCreate.file );
    }, function (newValue) {
        if(typeof newValue !=  "undefined" && newValue != null)
            blogCreate.upload([blogCreate.file]);
    });

    blogCreate.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: 'api/blog/image',
                    file: file,
                    headers : {
                        'Authorization' : authService.getToken(),
                        'Content-Type': file.type

                    }
                }).progress(function (evt) {
                    blogCreate.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                }).success(function (data) {
                    blogCreate.imageArray.push(data);
                }).error(function (data, status) {
                    console.log('error status: ' + status);
                })
            }
        }
    };


    blogCreate.title = "" ;
    blogCreate.body = ""  ;
    blogCreate.rawTags = "";
    blogCreate.rawDate = "";

    blogCreate.create = function () {

        var data = {
            title : blogCreate.title,
            body : CKEDITOR.instances.editor.getData(),
            tags : processRawTags(blogCreate.rawTags),
            date : processRawDate(blogCreate.rawDate),
            image : blogCreate.image
        };
        if (blogCreate.summary) {
            data.summary = blogCreate.summary.length;
        } else {
            data.summary = 0;
        }

        blogService.createPost(data);
    };
    function processRawTags(rawTags){
        if(rawTags === "")
            return [];
        return rawTags.split(",");
    }
    function processRawDate(rawDate) {
        if (rawDate === "")
            return new Date();
        var inputArray = rawDate.split("-");
        return new Date(inputArray[0],inputArray[1], inputArray[2],0,0,0,0);


    }

    $scope.$on('$destroy', function() {
        blogCreate.fileWatcher();
    });
}